import Control.Applicative hiding ( (<|>), many )

type Err = String
data Parser a = Parser { parse::String -> Either Err (a,String)  }

instance Functor Parser where
  fmap f p = Parser $ \s -> case parse p s of
    Left err -> Left err
    Right (x, rest) -> Right (f x, rest)
instance Applicative Parser where
  pure a = Parser $ \s -> Right (a,s)
  (<*>) pf pa = Parser $ \s -> case parse pf s of
    Left err -> Left err
    Right (f,r1) -> case parse pa r1 of
      Left err -> Left err
      Right (x,r2) -> Right (f x, r2)

anyChar::Parser Char
anyChar = Parser $ \s -> case s of
  [] -> Left "No chars left"
  (a:as) -> Right (a,as)

many :: Parser a -> Parser [a]
many p = Parser $ \s -> case parse p s of
  Left _ -> Right ([],s)
  Right (a,r1) -> case parse (many p) r1 of
    Left _ -> Right ([a],r1)
    Right (as,r2) -> Right(a:as, r2)

matching :: (Char -> Bool) ->  Parser Char
matching b = Parser $ \s -> case s of
  [] -> Left "No chars left"
  (a:as) | b a ->  Right ( a,as )
         | otherwise -> Left $ "Char " ++ [a] ++ " is wrong"

digit :: Parser Char
digit = matching (\c -> '0'<=c && c<='9')

char :: Char -> Parser Char
char c = matching (==c)

number :: Parser Int
number = pure read <*> many digit

(<|>) :: Parser a -> Parser a -> Parser a
(<|>) pa pb = Parser $ \s -> case parse pa s of
  Left err -> parse pb s
  Right a -> Right a

data Term = Var String
          | Lam String Term
          | App Term Term
          deriving (Eq)

instance Show Term where
  show t = case t of
    Var x -> x
    Lam x t -> "\\" ++ x ++ " . " ++ show t
    App t1 t2 -> "(" ++ show t1 ++ " " ++ show t2 ++ ")"

term :: Parser Term
term = lam <|> (app <|> var)

lam =  (\_ v _ _ _ t -> Lam v t)
   <$> char '\\'
   <*> (many $ matching (\c -> c <= 'z' && c >= 'a' ))
   <*> char ' '
   <*> char '.'
   <*> char ' '
   <*> term

app =  (\_ t1 _ t2 _ -> App t1 t2)
   <$> char '('
   <*> term
   <*> char ' '
   <*> term
   <*> char ')'

var = Var <$> (many $ matching (\c -> c <= 'z' && c >= 'a' ))

toTerm :: Either Err (Term,String) -> Term
toTerm p = case p of
  Right (a,s) -> a

canBeEvaluated :: Term -> Bool
canBeEvaluated t = case t of
  App (Lam v t1) t2 -> True
  App t1 t2 -> canBeEvaluated t1 || canBeEvaluated t2
  Lam v t -> canBeEvaluated t
  otherwise -> False

getVars = (\t -> case t of
  Var x -> (\c -> c == x)
  App t1 t2 -> (\c -> (getVars t1) c || (getVars t2) c)
  _ -> (\c -> False))

replaceVar = \t v1 v2 -> case t of
  Var v -> Var (if v1 == v then v2 else v)
  Lam v t2 -> if v1 == v then (Lam v2 (replaceVar t2 v v2) ) else (Lam v (replaceVar t2 v1 v2))
  App t1 t2 -> App (replaceVar t1 v1 v2) (replaceVar t2 v1 v2)

newVar = \x k -> if k (x++x) then newVar (x++x) k else x++x

eval1' :: Term -> (String -> Bool) -> Term

eval1' t k = if canBeEvaluated t then eval1' (case t of
  App (Lam v t1) t2 -> case t1 of
    App t3 t4 -> App (eval1' (App (Lam v t3) t2) k) (eval1' (App (Lam v t4) t2) k)
    Var v1 -> if v1 == v then t2 else t1
    Lam v1 t3 -> if ((getVars t2) v1) then
      Lam (newVar v1 (getVars t2)) (eval1' (App (Lam v (replaceVar t3 v1 (newVar v1 (getVars t2)))) t2) (\c -> k c || (getVars t2) c)  )
      else Lam v1 (eval1' (App (Lam v t3) t2) k)
  App t1 t2 -> App (eval1' t1 k) (eval1' t2 k)
  Lam v  t1 -> Lam v (eval1' t1 k)
  Var x -> Var (if k x then newVar x k else x) ) k else t

eval :: Term -> Term
eval t = eval1' t (\a -> False )

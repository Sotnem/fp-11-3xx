{-==== Сабитов Алмаз, 11-304 ====-}

module Trouble
       ( rocket
       , Load(..)
       , Spaceship(..)
       , orbiters
       , Phrase(..)
       , finalFrontier
       ) where

import Data.Function
import Data.List

{- Дан список ступеней ракеты, каждая ступень описана
парой «тяга - масса»

   Необходимо оставить в списке только одну ступень, с
максимальным отношением тяги к массе, если масса этой
ступени равна минимальной, иначе вернуть исходный список.

λ> rocket [(120,2),(10,2),(60,14),(90,30),(5,2),(10000,9999)]
[(120,2)]
λ> rocket [(120,20),(10,2),(60,14),(90,30),(5,2),(10000,9999)]
[(120,20),(10,2),(60,14),(90,30),(5,2),(10000,9999)]

   Все деления производить нацело.
 -}
rocket :: [(Integer, Integer)] -> [(Integer, Integer)]
rocket xs = error "Design rocket!"

getMaxRatio :: [(Integer, Integer)] -> (Integer, Integer)
getMaxRatio (x:[]) = x
getMaxRatio (x:xs) = if ((fst x) `quot` (snd x)) > (fst (getMaxRatio xs)) `quot` (snd (getMaxRatio xs)) then x
    else getMaxRatio xs


getMinMass :: [(Integer, Integer)] -> Integer
getMinMass (x:[]) = snd x
getMinMass (x:xs) | snd x < getMinMass xs = snd x
                  | otherwise = getMinMass xs


rocket :: [(Integer, Integer)] -> [(Integer, Integer)]
rocket xs = if snd (getMaxRatio xs) == getMinMass xs then [getMaxRatio xs] else xs

{- Космический корабль (КК) может состоять из:
 * ракеты, запускающей на заданную высоту несколько КК
 * полезной нагрузки из нескольких:
   - орбитальных аппаратов
   - посадочных зондов
 -}
data Load a = Orbiter a         -- орбитальный аппарат
            | Probe a           -- посадочный зонд
            deriving (Eq,Ord,Show)
data Spaceship a = Rocket Int [Spaceship a] -- высота и список кораблей
                 | Cargo [Load a]           -- полезная нагрузка
                 deriving (Eq,Show)

{- Дан список КК

   Вернуть список всех орбитальных аппаратов

λ> orbiters [Rocket 300 [Rocket 200 [Cargo [Orbiter "LRO", Probe "Lunokhod"]], Cargo [Orbiter "ISS"]]]
[Orbiter "ISS", Orbiter "LRO"]
 -}

getOrbiters :: [Load a] -> [Load a]
getOrbiters [] = []
getOrbiters ((Orbiter x):xs) = (Orbiter x):(getOrbiters xs)
getOrbiters ((Probe x):xs) = getOrbiters xs

orbiters :: [Spaceship a] -> [Load a]
orbiters [] = []
orbiters ((Cargo x):xs) = (orbiters xs) ++ (getOrbiters x)
orbiters ((Rocket _ x):xs) = (orbiters xs) ++ (orbiters x)


{- Даны фразы, нужно выдать того, кто сказал её

   instance Eq - не задан!
-}

data Phrase = Warp Int           -- Kirk:  Warp X, Mr. Sulu
            | BeamUp String      -- Kirk:  Beam X up, Scotty
            | IsDead String      -- McCoy: X is dead, Jim!
            | LiveLongAndProsper -- Spock: Live long and prosper
            | Fascinating        -- Spock: Fascinating!
            deriving Show

{-
λ> finalFrontier [IsDead "He", Fascinating, Warp 3, LiveLongAndProsper]
["McCoy", "Spock", "Kirk", "Spock"]
-}
finalFrontier :: [Phrase] -> [String]
finalFrontier [] = []
finalFrontier ((Warp _):xs) = "Kirk":(finalFrontier xs)
finalFrontier ((BeamUp _):xs) = "Kirk":(finalFrontier xs)
finalFrontier ((IsDead _):xs) = "McCoy":(finalFrontier xs)
finalFrontier ((LiveLongAndProsper):xs) = "Spock":(finalFrontier xs)
finalFrontier ((Fascinating):xs) = "Spock":(finalFrontier xs)

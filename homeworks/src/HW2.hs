-- Тесты чуть позже

module HW2
       ( Contact (..)
       , isKnown
       , Term (..)
       , eval
       , simplify
       ) where

data Contact = On
             | Off
             | Unknown

isKnown :: Contact -> Bool
isKnown Unknown = False
isKnown _ = True


data Term = Mult Term Term      -- умножение
          | Add Term Term       -- сложение
          | Sub Term Term       -- вычитание
          | Const Int           -- константа
            deriving (Eq,Show)
eval :: Term -> Int
eval (Mult x y) = (eval x) * (eval y)
eval (Add x y)  = (eval x) + (eval y)
eval (Sub x y)  = (eval x) - (eval y)
eval (Const x)  = x

-- Раскрыть скобки
-- Mult (Add (Const 1) (Const 2)) (Const 3) ->
-- Add (Mult (Const 1) (Const 3)) (Mult (Const 2) (Const 3))
-- (1+2)*3 -> 1*3+2*3

simplify :: Term -> Term
simplify t = if (case t of
  (Mult (Add a b) c) -> Add (Mult (simplify a) (simplify c)) (Mult (simplify b) (simplify c))
  (Mult c (Add a b)) -> Add (Mult (simplify a) (simplify c)) (Mult (simplify b) (simplify c))
  (Mult (Sub a b) c) -> Sub (Mult (simplify a) (simplify c)) (Mult (simplify b) (simplify c))
  (Mult c (Sub a b)) -> Sub (Mult (simplify a) (simplify c)) (Mult (simplify b) (simplify c))
  (Mult a b) -> Mult (simplify a) (simplify b)
  (Add a b) -> Add (simplify a) (simplify b)
  (Sub a b) -> Sub (simplify a) (simplify b)
  _ -> t ) == t then t else simplify t

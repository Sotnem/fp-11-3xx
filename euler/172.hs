factorial n = product [1..toInteger n]

fallingFactorial x n = product [x - fromInteger i | i <- [0..toInteger n - 1] ]

choose n k = fallingFactorial n k `div` factorial k

-- how many numbers can we get having d digits and p positions
p172 0 _ = 0
p172 d p
    | p < 4 = d^p
    | otherwise =
        (p172' p) +  p*(p172' (p-1)) + (choose p 2)*(p172' (p-2)) + (choose p 3)*(p172' (p-3))
    where
    p172' = p172 (d-1)

problem_172= (p172 10 18) * 9 `div` 10

primes = [2,3,5,7,11,13,17,19,23,29,31,37,41,43,47,53,59,61,67,71,73]
series _ 1 = [[0]]
series xs n = [x:ps|x<-xs,ps<-series [0..x] (n-1) ]
distinct =product. map (+1)
sumpri x = product $zipWith (^) primes x
prob x y = minimum[(sumpri m ,m)|m<-series [1..3] x,(>y)$distinct$map (*2) m]

problem_110 = prob 13 (8*10^6)

problem_166 =
    sum [ product (map count [[0, c, b-d, a-b-d],
            [0, b-a, c+d-a, b+d-a],
            [0, -b-c, a-b-c-d, -c-d],
            [0, a, d, c+d]])|
        a <- [-9..9],
        b <- [-9+a..9+a],
        c <- [-9..9],
        d <- [-9+a-c..9+a-c]]
    where
    count xs
        |u<l=0
        |otherwise=u-l+1
        where
        l = -minimum xs
        u = 9-maximum xs
